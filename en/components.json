[{
    "_id": "c-10",
    "_parentId": "b-10",
    "_type": "component",
    "_component": "twoColumn",
    "_classes": "clearFix",
    "_layout": "full",
    "title": "Introduction",
    "displayTitle": "",
    "body": "",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": true
    },
    "fontSize": "16px",
    "lineHeight": "",
    "fontColor": "#ffffff",
    "leftColumnWidth": "35%",
    "rightColumnWidth": "65%",
    "marginTop": "0px",
    "imageLayout": "left",
    "text": "We've taken what we've learned from the 2 billion people using our platforms<sup>1</sup>, and the 5 million businesses that advertise with us<sup>2</sup>, to create Facebook IQ, our insights platform. It was created to help you better understand your audience and develop campaigns that resonate with the people you want to reach.<br><br>In this course, you'll learn about the three insights solutions that make up Facebook IQ — studies, tools, and resources. We'll show you how to use these solutions individually or combined, depending on your business goals.",
    "_graphic": {
      "alt": "Woman in office looking at a computer screen",
      "src": "course/en/images/01_introduction/01_module_one_cover.png",
      "attribution": ""
    },
    "imageHeading": "",
    "imageSubHeading": ""
  },
  {
    "_id": "c-20",
    "_parentId": "b-10",
    "_type": "component",
    "_component": "text",
    "_classes": "",
    "_layout": "full",
    "title": "",
    "displayTitle": "",
    "body": "<sup>1</sup><cite>Facebook Earnings Report, Q2 2017</cite><br><sup>2</sup><cite>Facebook Data, April 2017</cite>",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": false
    }
  },
  {
    "_id": "c-30",
    "_parentId": "b-20",
    "_type": "component",
    "_component": "flexbox-list",
    "_classes": "",
    "_layout": "full",
    "title": "Facebook IQ insights solutions",
    "displayTitle": "Facebook IQ insights solutions",
    "body": "",
    "_mainflex": "main",
    "show_header_boolean": false,
    "header_title": "Header Title",
    "header_fontsize": "38",
    "bodytitle_fontsize": "24",
    "body_fontsize": "16",
    "header_fontcolor": "#000000",
    "bodytitle_fontcolor": "#000000",
    "body_fontcolor": "#000000",
    "body_color": "#ffffff",
    "header_image": "course/en/images/flexboxlist/header_img.jpg",
    "instruction": "",
    "titleAnimation": "transition.slideRightIn",
    "bodyAnimation": "transition.slideLeftIn",
    "_pageLevelProgress": {
      "_isEnabled": true
    },
    "_items": [{
      "_row": [{
          "item_title": "STUDIES",
          "item_body": "People<br>Advertising<br>Verticals",
          "item_image": "course/en/images/01_introduction/02_1_studies.png"
        },
        {
          "item_title": "TOOLS",
          "item_body": "Cross Border Insights Finder<br>Insights to Go<br>Audience Insights tool",
          "item_image": "course/en/images/01_introduction/02_2_tools.png"
        },
        {
          "item_title": "RESOURCES",
          "item_body": "Creative Hub<br>Facebook Business<br>Instagram Business<br>Facebook Blueprint",
          "item_image": "course/en/images/01_introduction/02_3_services.png"
        }

      ]
    }]
  },
  {
    "_id": "c-40",
    "_parentId": "b-30",
    "_type": "component",
    "_component": "co-jump",
    "_classes": "color-test",
    "_preLinkBody": "Next up: ",
    "_linkBody": "Learn to navigate and use Facebook IQ research studies",
    "_postLinkBody": ".",
    "_link": "#/id/co-100",
    "_graphic": "adapt/css/assets/icon_nextPage.png",
    "_pageLevelProgress": {
      "_isEnabled": false
    }
  },
  {
    "_id": "c-50",
    "_parentId": "b-40",
    "_type": "component",
    "_component": "twoColumn",
    "_classes": "clearFix",
    "_layout": "full",
    "title": "Facebook IQ Research Studies Introduction",
    "displayTitle": "",
    "body": "",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": true
    },
    "fontSize": "16px",
    "lineHeight": "",
    "fontColor": "#ffffff",
    "leftColumnWidth": "35%",
    "rightColumnWidth": "65%",
    "marginTop": "0px",
    "imageLayout": "right",
    "text": "To create Facebook IQ Studies, we begin with anonymized, aggregate data on the 2 billion people who use our platform. This gives the who, what, and how. Then we partner with third-party research companies to reveal the why behind people’s behaviors and motivations.<br><br>Studies can help you understand people’s behaviors, attitudes, and values, including their relationships with brands.",
    "_graphic": {
      "alt": "3 woman in an office looking at a computer screen",
      "src": "course/en/images/02_facebook_iq_research_studies/01_module_two_cover.png",
      "attribution": ""
    },
    "imageHeading": "",
    "imageSubHeading": ""
  },
  {
    "_id": "c-60",
    "_parentId": "b-40",
    "_type": "component",
    "_component": "accordion",
    "_classes": "",
    "_layout": "full",
    "title": "",
    "displayTitle": "",
    "body": "Studies are divided into three focus areas:",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": false
    },
    "_items": [{
        "title": "<b>People Insights</b>",
        "body": "takes a look at understanding consumer behavior across generations, locations, devices, and time.<br><br>Examples of past studies include a look at spring/summer fashion trends on Instagram, shifts in food culture on Facebook, and how people move between mobile and TV.",
        "_graphic": {
          "src": "",
          "alt": ""
        }
      },
      {
        "title": "<b>Advertising Insights</b>",
        "body": "examines the role of measurement in campaigns, and its influence and value in developing effective marketing.<br><br>Examples of past studies include how to craft effective stories in your campaigns, how to define your most important metrics, and how streaming services have affected traditional advertising..",
        "_graphic": {
          "src": "",
          "alt": ""
        }
      },
      {
        "title": "<b>Vertical Insights</b>",
        "body": "analyzes consumer behavior in specific industries, including automotive, consumer packaged goods, entertainment and media, financial services, gaming, retail, technology and connectivity, and travel.<br><br>Examples of past studies include the growth of auto buyers who prefer mobile shopping experiences, global mobile gaming trends, and examinations of the planning habits of international visitors to China.",
        "_graphic": {
          "src": "",
          "alt": ""
        }
      }
    ]
  },
  {
    "_id": "c-70",
    "_parentId": "b-50",
    "_type": "component",
    "_component": "graphic",
    "_classes": "",
    "_layout": "full",
    "title": "Studies",
    "displayTitle": "",
    "body": "To access Studies, go to the <b><a href='https://facebook.com/iq' target='_blank'>Facebook IQ platform</a></b> and on the top navigation bar, click on the type of insights you want to learn more about.",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": true
    },
    "_graphic": {
      "alt": "Insightss olutions screenshot",
      "src": "course/en/images/02_facebook_iq_research_studies/02_insightssolutions_studies.png",
      "attribution": ""
    }
  },
  {
    "_id": "c-80",
    "_parentId": "b-60",
    "_type": "component",
    "_component": "graphic",
    "_classes": "",
    "_layout": "full",
    "title": "",
    "displayTitle": "",
    "body": "From there, you can filter the Studies further by subcategory, or scroll through to view all studies related to that focus area.",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": false
    },
    "_graphic": {
      "alt": "Insights categories screenshot",
      "src": "course/en/images/02_facebook_iq_research_studies/03_studies_insightscategories.png",
      "attribution": ""
    }
  },
  {
    "_id": "c-100",
    "_parentId": "b-80",
    "_type": "component",
    "_component": "graphic",
    "_classes": "",
    "_layout": "full",
    "title": "Articles",
    "displayTitle": "",
    "body": "You can also access studies by clicking on <strong>Find Articles</strong> in the top right corner of the navigation bar. Here, you'll be able to filter the research studies by insights, verticals, region, platform and content type.",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": true
    },
    "_graphic": {
      "alt": "Find articles seach button example",
      "src": "course/en/images/02_facebook_iq_research_studies/04_findarticles_searchbutton.png",
      "attribution": ""
    }
  },
  {
    "_id": "c-110",
    "_parentId": "b-90",
    "_type": "component",
    "_component": "graphic",
    "_classes": "",
    "_layout": "full",
    "title": "",
    "displayTitle": "",
    "body": "",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": false
    },
    "_graphic": {
      "alt": "Find articles filter studies screenshot",
      "src": "course/en/images/02_facebook_iq_research_studies/05_findarticles_filterstudies.png",
      "attribution": ""
    }
  },
  {
    "_id": "c-120",
    "_parentId": "b-100",
    "_type": "component",
    "_component": "text",
    "_classes": "",
    "_layout": "full",
    "title": "",
    "displayTitle": "",
    "body": "Studies highlight shifts in people's behaviors, attitudes and values. By understanding your audience better, you can drive impactful results by creating relevant campaigns that speak to them.<br><br>You will often find that more than one article or study will offer insights that will be useful to your marketing campaign. For example, a brand developing a campaign to boost holiday sales can benefit from studies in <strong>People Insights > Moments</strong> subcategory, and the <strong>Vertical Insights > Retail</strong> subcategory.<br><br>We are always refreshing our content and releasing new articles, so it's a good idea to visit often, and also subscribe to our newsletter.",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": false
    }
  },
  {
    "_id": "c-130",
    "_parentId": "b-110",
    "_type": "component",
    "_component": "co-jump",
    "_classes": "color-test",
    "_preLinkBody": "Next up: Learn about the different",
    "_linkBody": "Facebook IQ tools and resources",
    "_postLinkBody": " that are available, and how to use them.",
    "_link": "#/id/co-150",
    "_graphic": "adapt/css/assets/icon_nextPage.png",
    "_pageLevelProgress": {
      "_isEnabled": false
    }
  },
  {
    "_id": "c-140",
    "_parentId": "b-120",
    "_type": "component",
    "_component": "twoColumn",
    "_classes": "clearFix",
    "_layout": "full",
    "title": "Facebook IQ Tools and Resources Introduction",
    "displayTitle": "",
    "body": "",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": true
    },
    "fontSize": "16px",
    "lineHeight": "",
    "fontColor": "#ffffff",
    "leftColumnWidth": "35%",
    "rightColumnWidth": "65%",
    "marginTop": "0px",
    "imageLayout": "right",
    "text": "You can use Facebook IQ's interactive tools to understand your audience, region and vertical better. These interactive tools include:<br><br><ul><li>Cross Border Insights Finder</li><li>Insights to Go</li><li>Audience Insights</li></ul><br>Let's take a closer look at each one.",
    "_graphic": {
      "alt": "3 woman in an office looking at a computer screen",
      "src": "course/en/images/03_facebook_iq_tools/01_module_three_cover.png",
      "attribution": ""
    },
    "imageHeading": "",
    "imageSubHeading": ""
  },
  {
    "_id": "c-150",
    "_parentId": "b-130",
    "_type": "component",
    "_component": "text",
    "_classes": "",
    "_layout": "full",
    "title": "Cross Border Insights Finder",
    "displayTitle": "Cross Border Insights Finder",
    "body": "",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": true
    }
  },
  {
    "_id": "c-160",
    "_parentId": "b-130",
    "_type": "component",
    "_component": "text",
    "_classes": "",
    "_layout": "right",
    "title": "",
    "displayTitle": "",
    "body": "Cross Border Insights Finder is a resource for cross-border businesses of all sizes to explore and unlock growth opportunities beyond their borders. Find and reach new audiences, wherever they may be, by comparing country data based on past campaign performances across our Facebook, Instagram and Audience Network platforms.<br><br>With the Cross Border Insights Finder, you can explore the worldview to identify, map and size up new growth opportunities around the world. You will be able to easily identify which markets are more likely to convert, cost less and encounter fewer ads, to better plan your international campaigns, whether they target a neighboring country or an entire region on the other side of the globe.",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": false
    }
  },
  {
    "_id": "c-170",
    "_parentId": "b-130",
    "_type": "component",
    "_component": "framed-video",
    "_classes": "",
    "_layout": "left",
    "title": "",
    "displayTitle": "",
    "body": "",
    "_videoPosition": "top",
    "_componentHeight": "610",
    "_videoPt": {
      "x": "0px",
      "y": "0px"
    },
    "_showControls": true,
    "_mute": true,
    "instruction": "",
    "_items": [{
        "video": "<video controls poster='course/en/video/03_facebook_iq_tools/01_live_demo_home_page_createad_video_demo_mobile_poster.png' id='vid' width='300px' controls='controls'><source src='course/en/video/03_facebook_iq_tools/01_live_demo_home_page_createad_video_demo_mobile.mp4'></video>"
      }

    ]
  },
  {
    "_id": "c-180",
    "_parentId": "b-135",
    "_type": "component",
    "_component": "text",
    "_classes": "",
    "_layout": "full",
    "title": "",
    "displayTitle": "",
    "body": "To access this tool, you can click on <b>Cross Border Insights Finder</b> under the <b>Tools & Resources</b> dropdown menu, or go to <a href='https://crossborderinsightsfinder.com' target='_blank'>https://crossborderinsightsfinder.com</a>. From there, you'll be able to filter for data based on your <strong>export market, industry</strong> and <strong>campaign objective</strong>.<br><br>If you're interested in learning more about expanding your business through Facebook, see how our reach, research and targeting tools work to help you connect with the right people and really scale your business in our course, <a href='https://fb.me/xbm-course' target='_blank'>Expand Across Borders with Facebook</a>.",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": false
    }
  },
  {
    "_id": "c-190",
    "_parentId": "b-140",
    "_type": "component",
    "_component": "media",
    "_classes": "",
    "_layout": "full",
    "title": "Insights to Go",
    "displayTitle": "Insights to Go",
    "body": "Insights to Go is a Facebook data discovery engine you can use to sort stats and proof points relevant to your campaign. You can sort and filtered by region, industry, audience segment, media channel, cultural moment and campaign strategy. You can access Insights to Go on any device, download data cards for presentations and share them directly from the tool.<br><br>To use Insights to Go:<br><br><ol><li>Hover over Tools & Resources in the top navigation bar, and click on Insights to Go.<li>Click on the <b>Get Started</b> button.</li><li>Apply filters or use the search bar to sort insights by <b>region</b>, <b>industry</b>, <b>people</b> (audience), <b>seasonal moment</b> and <b>campaign planning</b> tactics.</li><li>Find Facebook stats that fuel your creative and campaign decision-making.</li><li>Click on a data card to download a slide version, share with a client, colleague or to Facebook or to explore other related Insights to Go cards, or Facebook IQ Studies.</li></ol><br><br>You can see these steps in action here:",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": true
    },
    "_setCompletionOn": "inview",
    "_useClosedCaptions": true,
    "_startLanguage": "en",
    "_media": {
      "mp4": "course/en/video/03_facebook_iq_tools/02_insights_to_go_explainer_video.mp4",
      "ogv": "",
      "poster": "course/en/video/03_facebook_iq_tools/02_insights_to_go_explainer_video_poster.png",
      "cc": [{
          "srclang": "en",
          "src": "course/en/video/03_facebook_iq_tools/02_insights_to_go_explainer_video.vtt"
        },
        {
          "srclang": "pl",
          "src": "course/en/video/03_facebook_iq_tools/02_insights_to_go_explainer_video_sub-pl.vtt"
        }
      ]
    },
    "_transcript": {
      "_setCompletionOnView": false,
      "_inlineTranscript": false,
      "_externalTranscript": false,
      "inlineTranscriptButton": "Transcript",
      "inlineTranscriptCloseButton": "Close Transcript",
      "inlineTranscriptBody": "Transcript body text should go here",
      "transcriptLinkButton": "Transcript",
      "transcriptLink": "assets/transcript.pdf"
    },
    "_playerOptions": {
      "poster": "",
      "showPosterWhenEnded": false,
      "defaultVideoWidth": 480,
      "defaultVideoHeight": 270,
      "videoWidth": -1,
      "videoHeight": -1,
      "defaultAudioWidth": 400,
      "defaultAudioHeight": 30,
      "defaultSeekBackwardInterval": "(media.duration * 0.05)",
      "defaultSeekForwardInterval": "(media.duration * 0.05)",
      "audioWidth": -1,
      "audioHeight": -1,
      "startVolume": 0.8,
      "loop": false,
      "autoRewind": true,
      "enableAutosize": true,
      "alwaysShowHours": false,
      "showTimecodeFrameCount": false,
      "framesPerSecond": 25,
      "autosizeProgress": true,
      "alwaysShowControls": false,
      "hideVideoControlsOnLoad": false,
      "clickToPlayPause": true,
      "iPadUseNativeControls": false,
      "iPhoneUseNativeControls": false,
      "AndroidUseNativeControls": false,
      "features": ["playpause", "current", "progress", "duration", "tracks", "volume", "fullscreen"],
      "isVideo": true,
      "enableKeyboard": true,
      "pauseOtherPlayers": true,
      "startLanguage": "",
      "tracksText": "mejs.i18n.t('Captions/Subtitles')",
      "hideCaptionsButtonWhenEmpty": true,
      "toggleCaptionsButtonWhenOnlyOne": false,
      "slidesSelector": ""
    }
  },
  {
    "_id": "c-200",
    "_parentId": "b-150",
    "_type": "component",
    "_component": "text",
    "_classes": "",
    "_layout": "full",
    "title": "Audience Insights",
    "displayTitle": "Audience Insights",
    "body": "Audience Insights allows you to explore data on the behaviors and interests of real people. This can help with campaign planning and/or to validate an insight. Learn the benefits of Audience Insights and how to access this tool in our <a href='https://fb.me/audience-insights-course' target='_blank'>Audience Insights</a> course.<br><br>Once you've become familiar with these tools, and used them to learn more about your audience, Facebook IQ offers several resources that can help you to bring your ideas to life.",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": true
    }
  },
  {
    "_id": "c-201",
    "_parentId": "b-151",
    "_type": "component",
    "_component": "graphic",
    "_classes": "",
    "_layout": "full",
    "title": "Resources",
    "displayTitle": "Resources",
    "body": "<h2>Creative Hub</h2><br><a href='https://business.facebook.com/ads/creativehub/home/' target='_blank'>Creative Hub</a> is a site where you can create mockups of your ads, test out different formats and placements, and can share these mockups with others.",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": true
    },
    "_graphic": {
      "alt": "Find articles seach button example",
      "src": "course/en/images/03_facebook_iq_tools/creative_hub.png",
      "attribution": ""
    }
  },
  {
    "_id": "c-202",
    "_parentId": "b-152",
    "_type": "component",
    "_component": "text",
    "_classes": "",
    "_layout": "full",
    "title": "",
    "displayTitle": "",
    "body": "<h2>Facebook Business</h2><br>The <a href='https://www.facebook.com/business' target='_blank'>Facebook Business</a> site contains both information and inspiration to help you set up your Facebook Business Page, and how to use all of Facebook's features and resources to connect with your customers.<br><br><h2>Instagram Business</h2><br><a href='https://business.instagram.com/' target='_blank'>Instagram Business</a> will help you to consider the unique visual approach that can make an Instagram ad connect with customers.<br><br><h2>Facebook Blueprint</h2><br><a href='https://www.facebookblueprint.com/' target='_blank'>Facebook Blueprint</a> is a proud part of Facebook IQ resources, always adding new courses and updating existing ones to help you find your audience, craft your message and understand the results of your campaigns.",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": false
    }
  },
  {
    "_id": "c-210",
    "_parentId": "b-160",
    "_type": "component",
    "_component": "co-jump",
    "_classes": "color-test",
    "_preLinkBody": "Next up:",
    "_linkBody": "Tying it all together",
    "_postLinkBody": " by combining Facebook IQ tools and resources.",
    "_link": "#/id/co-200",
    "_graphic": "adapt/css/assets/icon_nextPage.png",
    "_pageLevelProgress": {
      "_isEnabled": false
    }
  },
  {
    "_id": "c-260",
    "_parentId": "b-210",
    "_type": "component",
    "_component": "twoColumn",
    "_classes": "clearFix",
    "_layout": "full",
    "title": "Tying It All Together Introduction",
    "displayTitle": "",
    "body": "",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": true
    },
    "fontSize": "16px",
    "lineHeight": "",
    "fontColor": "#ffffff",
    "leftColumnWidth": "35%",
    "rightColumnWidth": "65%",
    "marginTop": "68px",
    "imageLayout": "left",
    "text": "There is no one way to use Facebook IQ when gathering insights. You can use studies, tools, and resources separately, altogether, or in different combinations. To begin, it's helpful to look at your campaign holistically.",
    "_graphic": {
      "alt": "Man in an open work space working on his computer",
      "src": "course/en/images/05_tying_it_all_together/01_module_five_cover.png",
      "attribution": ""
    },
    "imageHeading": "",
    "imageSubHeading": ""
  },
  {
    "_id": "c-270",
    "_parentId": "b-220",
    "_type": "component",
    "_component": "animate-text-blocks",
    "_classes": "",
    "_layout": "full",
    "title": "",
    "displayTitle": "",
    "body": "",
    "instruction": "",
    "backgroundColor": "#d0dbfa,#d0dbfa,#d0dbfa,#d0dbfa,#d0dbfa,#d0dbfa",
    "_animationOnView": "slide,fade,stagger",
    "_cornerRadius": "4px",
    "_spacing": "10px",
    "_pageLevelProgress": {
      "_isEnabled": false
    },
    "_items": [{
        "item_title": "",
        "body": "1. Establish your end goal. What are you trying to accomplish with your campaign?"
      },
      {
        "item_title": "",
        "body": "2. Identify what kind of information you need to accomplish this goal."
      },
      {
        "item_title": "",
        "body": "3. Use Facebook IQ's insight solutions (Studies, Tools, Resources) to find this information."
      }
    ]
  },
  {
    "_id": "c-280",
    "_parentId": "b-230",
    "_type": "component",
    "_component": "text",
    "_classes": "",
    "_layout": "full",
    "title": "",
    "displayTitle": "",
    "body": "Let’s take a look at two hypothetical situations…",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": false
    }
  },
  {
    "_id": "c-290",
    "_parentId": "b-240",
    "_type": "component",
    "_component": "twoColumn",
    "_classes": "clearFix",
    "_layout": "full",
    "title": "The small business owner",
    "displayTitle": "The small business owner",
    "body": "",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": true
    },
    "fontSize": "16px",
    "lineHeight": "",
    "fontColor": "#ffffff",
    "leftColumnWidth": "35%",
    "rightColumnWidth": "65%",
    "marginTop": "12px",
    "imageLayout": "right",
    "text": "Camy is the owner of Jasper's Boutique, an internet-only, mail order fashion store. One of the most important things for her business is following fashion trends so she can use her time and resources wisely.<br><br>Camy follows fashion blogs to spot new trends and decide what to feature in her online store. She currently advertises on Facebook, and thanks to the Audience Insights tool she's very familiar with her customers, but Camy wants her next campaign to reach and connect with new people in order to grow her business.",
    "_graphic": {
      "alt": "Woman takes picture of scarfs in a box using her mobile phone",
      "src": "course/en/images/05_tying_it_all_together/02_small_business_owner.png",
      "attribution": ""
    },
    "imageHeading": "",
    "imageSubHeading": ""
  },
  {
    "_id": "c-300",
    "_parentId": "b-250",
    "_type": "component",
    "_component": "flipCard",
    "_classes": "",
    "_layout": "full",
    "title": "",
    "displayTitle": "",
    "body": "Click the blue cards below to see Camy's journey using Facebook IQ:",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": false
    },
    "_items": [{
        "frontImageSrc": "",
        "frontHeading": "<center>Insight</center>",
        "fontSizeHeading": "24px",
        "fontSizeText": "14px",
        "frontImgTop": "24px",
        "frontImgbottom": "",
        "frontImgleft": "",
        "frontImgright": "",
        "frontImgWidth": "30%",
        "frontImgHeight": "",
        "frontImgOpacity": "1",
        "padding": "12px 24px",
        "fontColor": "#ffffff",
        "frontListOfOptions": [
          ""
        ],
        "frontFooter": "",
        "frontFooterColor": "#808080",
        "frontFooterPadding": "12px",
        "backHeading": "",
        "backListOfOptions": [
          "Camy searches the Studies for recent articles about fashion on the Facebook network. She discovers an article in People Insights featuring Eva Chen, former editor-in-chief of Lucky Magazine. Eva talks about how brands are using Instagram to give their customers an inside perspective on their latest lines, and Camy starts to consider advertising on Instagram. Then she reads the Advertising Insights article <i>Optimizing Audience Buying on Facebook and Instagram,</i> which suggests that by advertising on both Facebook and Instagram, she will reach a larger audience as well as use her marketing budget more efficiently."
        ],
        "textShadow": ""
      },
      {
        "frontImageSrc": "",
        "frontHeading": "<center>Action</center>",
        "fontSizeHeading": "24px",
        "fontSizeText": "14px",
        "frontImgTop": "24px",
        "frontImgbottom": "",
        "frontImgleft": "",
        "frontImgright": "",
        "frontImgWidth": "30%",
        "frontImgHeight": "",
        "frontImgOpacity": "1",
        "padding": "12px 24px",
        "fontColor": "#ffffff",
        "frontListOfOptions": [
          ""
        ],
        "frontFooter": "",
        "frontFooterColor": "#808080",
        "frontFooterPadding": "12px",
        "backHeading": "",
        "backListOfOptions": [
          "Based upon what she has read, Camy decides that in addition to building Jasper's Boutique's presence on Instagram, she will connect with fashionistas and influencers on the platform. This also includes expanding her advertising from just Facebook to include Instagram as well."
        ],
        "textShadow": ""
      }
    ],
    "_flipType": "singleFlip",
    "_flipTime": 400
  },
  {
    "_id": "c-310",
    "_parentId": "b-260",
    "_type": "component",
    "_component": "text",
    "_classes": "",
    "_layout": "full",
    "title": "",
    "displayTitle": "",
    "body": "With the studies and insights on Facebook IQ, Camy has found a way to reach more potential customers, and particularly ones that are as passionate about fashion as her.",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": false
    }
  },
  {
    "_id": "c-350",
    "_parentId": "b-300",
    "_type": "component",
    "_component": "twoColumn",
    "_classes": "clearFix",
    "_layout": "full",
    "title": "The rising-star chef",
    "displayTitle": "The rising-star chef",
    "body": "",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": true
    },
    "fontSize": "16px",
    "lineHeight": "",
    "fontColor": "#ffffff",
    "leftColumnWidth": "35%",
    "rightColumnWidth": "65%",
    "marginTop": "45px",
    "imageLayout": "right",
    "text": "Alberto is a chef in Rome, Italy, who has invented unique pasta dishes. He has become famous thanks to foodies sharing images of his beautiful meals on Instagram and international visitors writing very positive reviews of his exclusive restaurant on Facebook. Now Alberto has to make an important decision about what he should do to further grow his business.",
    "_graphic": {
      "alt": "Person at dinner looking at a mobile phone",
      "src": "course/en/images/05_tying_it_all_together/04_rising_star_chef.png",
      "attribution": ""
    },
    "imageHeading": "",
    "imageSubHeading": ""
  },
  {
    "_id": "c-360",
    "_parentId": "b-310",
    "_type": "component",
    "_component": "flipCard",
    "_classes": "",
    "_layout": "full",
    "title": "",
    "displayTitle": "",
    "body": "Click the blue cards below to see Alberto's journey using Facebook IQ:",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": false
    },
    "_items": [{
        "frontImageSrc": "",
        "frontHeading": "<center>Insight</center>",
        "fontSizeHeading": "24px",
        "fontSizeText": "16px",
        "frontImgTop": "24px",
        "frontImgbottom": "",
        "frontImgleft": "",
        "frontImgright": "",
        "frontImgWidth": "30%",
        "frontImgHeight": "",
        "frontImgOpacity": "1",
        "padding": "12px 24px",
        "fontColor": "#ffffff",
        "frontListOfOptions": [
          ""
        ],
        "frontFooter": "",
        "frontFooterColor": "#808080",
        "frontFooterPadding": "12px",
        "backHeading": "",
        "backListOfOptions": [
          "Alberto uses <b>Audience Insights</b> to learn specifics about the people who follow his restaurant's Facebook Page. He discovers that very few of his followers are in Italy; the majority are actually from other countries in the EU. Drilling deeper, he finds that the majority are interested in cooking at home and gourmet food. He turns to Facebook IQ for studies about the eating habits of people on Facebook and Instagram. After reading <i>Three Shifts in Food Culture</i> he forms a theory that meal kits could be a way to further expand his brand. With this, Alberto turns to <b>Cross-Border Insights</b> to compare potential costs of advertising and potential competition in other countries."
        ],
        "textShadow": ""
      },
      {
        "frontImageSrc": "",
        "frontHeading": "<center>Action</center>",
        "fontSizeHeading": "24px",
        "fontSizeText": "14px",
        "frontImgTop": "24px",
        "frontImgbottom": "",
        "frontImgleft": "",
        "frontImgright": "",
        "frontImgWidth": "30%",
        "frontImgHeight": "",
        "frontImgOpacity": "1",
        "padding": "12px 24px",
        "fontColor": "#ffffff",
        "frontListOfOptions": [
          ""
        ],
        "frontFooter": "",
        "frontFooterColor": "#808080",
        "frontFooterPadding": "12px",
        "backHeading": "",
        "backListOfOptions": [
          "Alberto first adds a Shop section to his Facebook Page and features a few meal kits that can be shipped to other countries. He then uses what he has learned from Audience Insights and Facebook IQ market studies to create a highly targeted campaign to sell meal kits to followers of his Page who enjoy cooking at home and food photography, and who live in Germany and France."
        ],
        "textShadow": ""
      }
    ],
    "_flipType": "singleFlip",
    "_flipTime": 400
  },
  {
    "_id": "c-370",
    "_parentId": "b-320",
    "_type": "component",
    "_component": "text",
    "_classes": "",
    "_layout": "full",
    "title": "",
    "displayTitle": "",
    "body": "Using all of the tools available to Facebook marketers and business owners, Alberto has gained greater insight into his customers, and a brand new way to grow his business that he might not have considered otherwise.<br><br>Use the studies, resources, and tools provided by Facebook IQ to help you better understand your audience and develop campaigns that resonate with the people you want to reach. Timely insights can be delivered to your inbox by subscribing to Facebook IQ's newsletter at <a href='http://facebook.com/iq' target='_blank'>facebook.com/iq</a>.",
    "instruction": "",
    "_pageLevelProgress": {
      "_isEnabled": false
    }
  },
  {
    "_id": "c-380",
    "_parentId": "b-330",
    "_type": "component",
    "_component": "co-jump",
    "_classes": "color-test",
    "_autoComplete": true,
    "_preLinkBody": "Click the button below to finish this course. Note: <a href='#'>click here</a> to verify that status bars for each module are complete.<br>",
    "_linkBody": "<div style = 'text-align:center;padding:8px;'><button class='buttons-action'><b>I'M FINISHED WITH THIS COURSE</b></button></div><br> ",
    "_postLinkBody": "",
    "_link": "",
    "_pageLevelProgress": {
      "_isEnabled": false
    }
  }

]
